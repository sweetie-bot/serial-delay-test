#include <getopt.h>
#include <stdlib.h>
#include <stdbool.h>
#include <errno.h>
#include <time.h>
#include <signal.h>
#include <unistd.h>
#include <fcntl.h>
#include <stdio.h>
#include <string.h>
#include <math.h>


int atoi_abort(const char * str, int min, int max, const char * error_msg) 
{
	long tmp;
	errno = 0;
	tmp = strtol(str, 0, 10);
	if (errno) {
		perror(error_msg);
		exit(EXIT_FAILURE);
	}
	if (tmp < min || tmp > max) {
		fprintf(stderr, "%s: number must be between %d and %d\n", error_msg, min, max);
		exit(EXIT_FAILURE);
	}
	return tmp;
}

double timespec_diff_sec(const struct timespec * stop, const struct timespec * start) 
{
	struct timespec result;
	if ((stop->tv_nsec - start->tv_nsec) < 0) {
        result.tv_sec = stop->tv_sec - start->tv_sec - 1;
        result.tv_nsec = stop->tv_nsec - start->tv_nsec + 1000000000;
    } else {
        result.tv_sec = stop->tv_sec - start->tv_sec;
        result.tv_nsec = stop->tv_nsec - start->tv_nsec;
    }
	return result.tv_sec + result.tv_nsec/1000000000.0;
}

	
#define STDIN_FD 0
#define MAX_FRAME_LENGTH 255

struct statistics_t {
	double mean;
	double cov;
	double max;
	unsigned long N;
};

unsigned int number_packets = 0;
unsigned int number_packets_max = 0;
bool dump_statistics = false;
bool exit_flag = false;
struct timespec startup_timespec;
struct statistics_t stat_frame_start_delay, stat_frame_end_delay;


void stat_add_sample(struct statistics_t * stat, double val) {
	double delta = val - stat->mean;
	stat->N++;
	stat->mean += delta / stat->N;
	stat->cov += delta * (val - stat->mean);
	
	if (val > stat->max) stat->max = val;
}

void stat_reset(struct statistics_t * stat) 
{
	memset(stat, 0, sizeof(struct statistics_t));
}

void stat_print(struct statistics_t * stat) 
{
	fprintf(stderr, "N: %lu avg: %lf std: %lf max: %lf", stat->N, stat->mean, sqrt(stat->cov / (stat->N-1)), stat->max);
}

void frame_parser(unsigned char c) {
	static enum frame_praser_state_t {
		START_BYTE1 = 0,
		START_BYTE2,
		LENGTH,
		BODY,
		FRAME_GOOD,
		FRAME_ERROR,
	} state = START_BYTE1;

	static char send_timespec_buffer[sizeof(struct timespec)];
	static unsigned int lenght;
	static unsigned int nbytes_received;
	static char checksum;

	static struct timespec recv_start_timespec;
	static struct timespec recv_end_timespec;

	// process next byte
	/*{
		unsigned int tmp1 = state;
		unsigned int tmp2 = c;
		fprintf(stderr, "state = %u, c = %x, n = %u, l = %u\n", tmp1, tmp2, nbytes_received, lenght);
	}*/
	checksum ^= c;
	switch (state) {
		case START_BYTE1:
			if (c == 0xFFu) {
				state = START_BYTE2;
				checksum = 0xFF;
				// get frame start timestamp
				clock_gettime(CLOCK_REALTIME, &recv_start_timespec);
			}
			break;

		case START_BYTE2:
			if (c == 0xFF) {
				state = LENGTH;
			}
			else {
				state = START_BYTE1;
			}
			break;

		case LENGTH:
			lenght = c;
			nbytes_received = 3;
			state = BODY;
			break;

		case BODY:
			if (nbytes_received < 3 + sizeof(struct timespec)) {
				send_timespec_buffer[nbytes_received - 3] = c;
			}
			nbytes_received++;
			if (nbytes_received == lenght) {
				if (checksum == 0) state = FRAME_GOOD;
				else state = FRAME_ERROR;
				// get frame end timespec
				clock_gettime(CLOCK_REALTIME, &recv_end_timespec);
			}
			break;
	}

	// check if full frame received
	// TODO mask signal
	if (state == FRAME_GOOD) {
		double t = timespec_diff_sec((struct timespec *) send_timespec_buffer, &startup_timespec);
		double delay_start = timespec_diff_sec(&recv_start_timespec, (struct timespec *) send_timespec_buffer);
		double delay_end = timespec_diff_sec(&recv_end_timespec, (struct timespec *) send_timespec_buffer);

		stat_add_sample(&stat_frame_start_delay, delay_start);
		stat_add_sample(&stat_frame_end_delay, delay_end);

		if (dump_statistics) {
			fprintf(stdout, "%lf %lf %lf\n", t, delay_start, delay_end);
		}

		number_packets++;
		state = START_BYTE1;
		//fprintf(stderr, "state = %u, t = %lf, d1 = %lf, d2 = %lf \n", state, t, delay_start, delay_end);
	}
	else if (state == FRAME_ERROR) {
		number_packets++;
		state = START_BYTE1;

		//fprintf(stderr, "state = %u\n", state);
	}

}

void process_input(void) 
{
	int retval;
	char buffer[MAX_FRAME_LENGTH];
	unsigned int buffer_read_size;
	fd_set fds;

	for(;;) {
		// wait for data and read it into buffer
		FD_ZERO(&fds);
		FD_SET(STDIN_FD, &fds);
		retval = select(STDIN_FD+1, &fds, NULL, NULL, NULL);
		if (retval == -1 && errno != EINTR) {
			perror("select() call error");
			exit(EXIT_SUCCESS);
		}

		// process new data if it is available
		if (FD_ISSET(STDIN_FD, &fds)) {
			buffer_read_size = read(STDIN_FD, buffer, MAX_FRAME_LENGTH);
			if (buffer_read_size == -1) {
				switch (errno) {
					case EINTR:
						// interupted by signal, should check exit_flag
						buffer_read_size = 0;
						break;

					case EAGAIN:
						// closed pipe? 
						exit_flag = true;
						buffer_read_size = 0;
						break;

					default:
						perror("read() call error");
						exit(EXIT_FAILURE);
				}
			}
			else if (buffer_read_size == 0) {
				// eof detected
				exit_flag = true;
			}

			// now process received data according to state machine
			for(int i = 0; i < buffer_read_size; i++) frame_parser(buffer[i]);

			// check if we received enough packets
			if (number_packets_max != 0 && number_packets >= number_packets_max) exit_flag = true;
		}

		// exit if exit flag is raised
		if (exit_flag) {
			fprintf(stderr, "Frame first byte delay:   "); stat_print(&stat_frame_start_delay);
			fprintf(stderr, "\nFrame last byte delay:    "); stat_print(&stat_frame_end_delay);
			fprintf(stderr, "\nTotal %u frames registered, %u erroneous frames detected.\n", number_packets, number_packets - stat_frame_end_delay.N);

			exit(EXIT_SUCCESS);
		}
	}
}

void sigterm_handler(int signum) 
{
	exit_flag = true;
}

int main(int argc, char * argv[]) 
{
	// parse arguments
	for(;;) {
		int opt = getopt(argc, argv, "n:s");
		if (opt == -1) break;

		switch (opt) {
			case 'n':
				number_packets_max = atoi_abort(optarg, 1, 1000000, "Unable to parse '-n' option");
				break;

			case 's':
				dump_statistics = true;
				break;

			case 'h':
			default:
				printf("%s [-n NUMBER ] [-s]\n", argv[0]);
				printf("\n");
				printf("Receive NUMBER of frames on stdin and print statistics to stderr on exit.\n");
				printf("If NUMBER is equal to zero or omitted then program exits when SIGTERM or SIGINT is received.\n");
				printf("If -s flag is specified delay information for each frame is dumped to stdout in following format\n");
				printf("\n");
				printf("TIME_FROM_START FIRST_BYTE_DELAY LAST_BYTE_DELAY\\n\n");
				printf("\n");
				printf("one line per frame.");
				printf("\n");
				printf("Input frames has following structure:\n");
				printf("Fields:      | 0xFF | 0xFF | LENGTH |    TIME_STAMP           |         random bytes              | checksum |\n");
				printf("Size (bytes) |  1   |  1   |   1    | sizeof(struct timespec) | LENGTH-(4+sizeof(struct timespec) |     1    |\n");
				printf("\n");
				printf("TIME_STAMP contains timespec structure from CLOCK_REALTIME. It marks the time when package was send.\n");
				printf("Checksum is XOR over all other fields. Maximal frame length is 255 bytes.\n");
				exit(EXIT_SUCCESS);
				break;
		}
	} 

	// set non-blocking mode for stdin
	{
		int flags = fcntl(STDIN_FD, F_GETFL);
		if (flags == -1) {
			perror("Unable to get mode flags fd 0");
			exit(EXIT_FAILURE);
		}
		if (fcntl(STDIN_FD, F_SETFL, flags | O_NONBLOCK) == -1) {
			perror("Unable to set non-blocking mode flag for fd 0");
			exit(EXIT_FAILURE);
		}
	}
	// setup sigterm handler
	signal(SIGTERM, sigterm_handler);
	signal(SIGINT, sigterm_handler);
	// get start timestamp
	clock_gettime(CLOCK_REALTIME, &startup_timespec);
	stat_reset(&stat_frame_start_delay);
	stat_reset(&stat_frame_end_delay);

	// process input
	process_input();
}

