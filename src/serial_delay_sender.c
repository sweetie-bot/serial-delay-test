#include <getopt.h>
#include <stdlib.h>
#include <errno.h>
#include <time.h>
#include <signal.h>
#include <unistd.h>
#include <stdio.h>
#include <string.h>

#define  MIN_PACKAGE_LENGTH  (2 + 1 + sizeof(struct timespec) + 1)

int port_fd;
unsigned int package_length = MIN_PACKAGE_LENGTH;
unsigned int number_packages = 1000;
char * package_buffer;

void timer_thread(union sigval arg)
{
	char checksum = 0;
	ssize_t retval, bytes_sent;
	struct timespec ts;

	// prepare package_buffer	
	package_buffer[0] = 0xff;
	package_buffer[1] = 0xff;
	package_buffer[2] = package_length;
	for(int i = package_length-12; i > 0; i--) package_buffer[i + 2 + sizeof(struct timespec) + 1] = rand();
	clock_gettime(CLOCK_REALTIME, &ts);
	memcpy(package_buffer + 3, &ts, sizeof(ts));
	for(int i = 0; i < package_length-1; i++) checksum ^= package_buffer[i];
	package_buffer[package_length-1] = checksum;

	// write 
	bytes_sent = 0;
	do {
		do {
			retval = write(1, package_buffer + bytes_sent, package_length - bytes_sent);
		} while (retval == -1 && errno == EINTR);
		if (retval == -1) {
			perror("Write to output stream failed.");
			exit(EXIT_FAILURE);
		}
		bytes_sent += retval;
	} while (bytes_sent < package_length);

	// decrement package counter if it is not equal to zero.
	if (number_packages > 0) {
		number_packages--;
		if (number_packages == 0) {
			exit(EXIT_SUCCESS);
		}
	};
}

int atoi_abort(const char * str, int min, int max, const char * error_msg) 
{
	long tmp;
	errno = 0;
	tmp = strtol(str, 0, 10);
	if (errno) {
		perror(error_msg);
		exit(EXIT_FAILURE);
	}
	if (tmp < min || tmp > max) {
		fprintf(stderr, "%s: number must be between %d and %d\n", error_msg, min, max);
		exit(EXIT_FAILURE);
	}
	return tmp;
}

int main(int argc, char * argv[]) 
{
	// parameters
	unsigned int interval_ms = 5;
	//const char * device = "/dev/ttyS0";
	// timer
	timer_t timer_id;
	struct itimerspec timer_interval;
	struct sigevent timer_event;

	// parse arguments
	for(;;) {
		int opt = getopt(argc, argv, "n:i:l:h");
		if (opt == -1) break;

		switch (opt) {
			case 'n':
				number_packages = atoi_abort(optarg, 0, 1000000, "Unable to parse '-n' option");
				break;

			case 'i':
				interval_ms = atoi_abort(optarg, 1, 1000, "Unable to parse '-i' option");
				break;

			case 'l':
				package_length = atoi_abort(optarg, MIN_PACKAGE_LENGTH, 255, "Unable to parse '-l' option");
				break;

			//case 'd':
				//device = getarg;

			case 'h':
			default:
				printf("%s [-n NUMBER ] [-i INTREVAL_MS ] [-l LENGTH]\n", argv[0]);
				printf("\n");
				printf("Send to standart output NUMBER of packages of length LENGTH bytes with interval INTREVAL_MS ms.\n");
				printf("If NUMBER is equal to zero send packages forever.\n");
				printf("\n");
				printf("Package has following structure:\n");
				printf("Fields:      | 0xFF | 0xFF | LENGTH |    TIME_STAMP           |         random bytes              | checksum |\n");
				printf("Size (bytes) |  1   |  1   |   1    | sizeof(struct timespec) | LENGTH-(4+sizeof(struct timespec) |     1    |\n");
				printf("\n");
				printf("TIME_STAMP contains timespec structure from CLOCK_REALTIME. It marks the time when package was send.\n");
				printf("Checksum is XOR over all other fields.\n");
				exit(EXIT_SUCCESS);
				break;
		}
	} 

	// create send buffer
	package_buffer = malloc(package_length);
	if (package_buffer == 0) {
		perror("Uable to create package_buffer");
		exit(EXIT_FAILURE);
	}

	// open output device
	/*if (serial_open(device, baudrate) == -1) {
		fprintf(stderr, "Unable to open file", error_msg, min, max);
		exit(EXIT_FAILURE);
	}*/

	fprintf(stderr, "Sending %u packages lenght of %u bytes with interval %u ms.\n", number_packages, package_length, interval_ms);

	// setup timer
	timer_event.sigev_notify = SIGEV_THREAD;
	timer_event.sigev_value.sival_ptr = &timer_id;
	timer_event.sigev_notify_function = timer_thread;
	timer_event.sigev_notify_attributes = NULL;

	if (timer_create(CLOCK_REALTIME, &timer_event, &timer_id) == -1) {
		perror("Uable to create timer");
		exit(EXIT_FAILURE);
	}

	timer_interval.it_value.tv_sec = interval_ms / 1000;
	timer_interval.it_value.tv_nsec = (interval_ms % 1000) * 1000000;
	timer_interval.it_interval = timer_interval.it_value;

	if (timer_settime(timer_id, 0, &timer_interval, 0) == -1) {
		perror("Uable to create timer");
		exit(EXIT_FAILURE);
	}

	// wait until timer thread calls exit()
	for (;;) pause();
}

