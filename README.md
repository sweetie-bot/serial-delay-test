serial-delay-test: serial port delay test
=========================================

`serial-delay-sender` and `serial-delay-delay` utilities may be used to measure serial port frame processing delay.
The first one periodically sends frames with timestamp (`CLOCK_REALTIME` is used), the second one receives frames,
calculate statistics. Via command line options you can adjust number of frames to be sent or received, interval and frame length.

This utility can be used to test a serial port in  loopback mode or any other pipe-like transport. 

NOTE: due to buffering you should start sender after receiver.

### Frame format

Frame format is architecture dependent.

    Fields:      | 0xFF | 0xFF | LENGTH |    TIME_STAMP           |         random bytes              | checksum |

`TIME_STAMP` contains `timespec` structure from `CLOCK_REALTIME`. It marks the time when package was send.
Checksum is XOR over all other fields.

## serial-delay-sender

    Usage: serial-delay-sender [-n NUMBER ] [-i INTREVAL_MS ] [-l LENGTH]

Send to stdout `NUMBER` of packages of length `LENGTH` bytes with interval `INTREVAL_MS` ms.
If `NUMBER` is equal to zero send packages forever.

Typical usage:

    serial-delay-sender -i 10 -l 100 >/dev/ttyS0

## serial-delay-receiver

    Usage: serial-delay-receiver [-n NUMBER ] [-s]

Receive NUMBER of frames on stdin and print statistics to stderr on exit.
If NUMBER is equal to zero or omitted then program exits when  SIGTERM or SIGINT is received.
If -s flag is specified delay information for each frame is dumped to stdout in following format

    TIME_FROM_START FIRST_BYTE_DELAY LAST_BYTE_DELAY

one line per frame. `TIME_FROM_START` is send time according to frame timestamp. 

User-readable output displays number of received frames, average delay, standard deviation 
and max delay.

Typical usage:

    serial-delay-sender -n 3000 -s >delay_meashurements.txt </dev/ttyS0

## serial-delay-test.sh

    Usage: serial-delay-test.sh TTY_DEVICE OUTFILE [ RT_PRI [ BAUDRATE ] ]
	   
Delay measurement data is written to `OUTFILE`.
Default `BAUDRATE` is 115200 baud. If `RT_PRI` is omitted then normal priority is used.

## INSTALL

    autoreconf -v
    ./configure
    make
    make install
