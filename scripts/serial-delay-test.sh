#!/bin/bash

if [ -z $1 ]; then 
	echo Usage: $0 TTY_DEVICE OUTFILE [ RT_PRI [ BAUDRATE ] ]
	echo
	echo Delay measurement data is written to OUTFILE.
	echo Default BAUDRATE is 115200 baud. If RT_PRI is omitted then normal priority is used.
	exit 0
fi

# parameters
TTY=$1
if [ $2 ]; then
	STAT_FLAG="-s"
	STAT_FILE=$2
else
	STAT_FILE="/dev/null"
fi
if [ $3 ]; then
	PREFIX="chrt -f $3"
fi
BAUD=${4:-115200}

# port configuration
stty -F $TTY $BAUD raw -echo
setserial $TTY low_latency

#start receiver
$PREFIX serial-delay-receiver  $STAT_FLAG <$TTY >$STAT_FILE &
RECEIVER_PID=$!

#start sender
$PREFIX serial-delay-sender -n 2000 -i 10 >$TTY

#stop test
kill $RECEIVER_PID
wait $RECEIVER_PID
