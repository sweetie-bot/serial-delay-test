#!/bin/bash
LOAD=noload
SPI=nospi

# parse command line
while [ $1 ]; do
	case $1 in
		load)
			LOAD=load
			;;
		noload)
			LOAD=
			;;
		spi)
			SPI=spi
			;;
		nospi)
			SPI=nospi
			;;
	esac
	shift
done

# decalre variables
PORTS="1 2"
RT_PRI=60
PIDS=""
KILL_PIDS=""

# start tests
echo "Serial port $LOAD $SPI test."

# start CPU load
if [ z"$LOAD" = z"load" ]; then
	hackbench -l 1000000 &
	KILL_PIDS="$!"
fi

# serial tests
for PORT in $PORTS; do 
	serial-delay-test.sh /dev/ttyS${PORT} stat-${LOAD}-port${PORT}.txt ${RT_PRI} &
	PIDS="$PIDS $!"
done

# wait for finish
for PID in $PIDS; do
	wait $PID
done

# kill background processes
for PID in $KILL_PIDS; do
	kill $PID
done

echo "Test done!"
