1;

function plot_hist(plot_title, delay)
	max_disp_delay=0.010;
	#hist(min(delay, max_disp_delay), 50)
	intervals = linspace(0, max_disp_delay, 51);
	bins = histc(min(delay, max_disp_delay), intervals);
	bar(intervals, bins);
	xlim([0 max_disp_delay]);
	grid on
	title(sprintf("%s: n: %d avg: %f std: %f max: %f", plot_title, numel(delay), mean(delay), std(delay), max(delay)))
end


argv_array = argv;
n_plots = length(argv_array);

if n_plots == 0
    disp("Usage: octave serial-delay-plot file1.txt [ file2.txt ... ]");
    disp("");
    disp("Plot delay hstogram.");
	exit(0);
end

for k=1:n_plots
	subplot(n_plots,1,k)
	data=load(argv_array{k},"-ascii");
	plot_hist(argv_array{k}, data(:,3));
end

kbhit()

print(strcat(argv_array{1},".pdf"), "-dpdf");
print(strcat(argv_array{1},".png"), "-dpng");
